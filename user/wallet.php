<?php
/* Este archivo debe manejar la lógica para obtener la información de la billetera */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
session_start();
$correo = $_SESSION["correo"];
echo $correo;

$sql="SELECT *
FROM Usuario
INNER JOIN (SELECT new.id_moneda,new.balance,new.nombre,new.sigla,Precio_moneda.valor,new.id_usuario
FROM Precio_moneda
INNER JOIN 
    (SELECT *
     FROM Usuario_tiene_moneda
     INNER JOIN
     Moneda
     ON Usuario_tiene_moneda.id_moneda=Moneda.id) as new
    ON Precio_moneda.id_moneda=new.id_moneda) as billetera
ON Usuario.id=billetera.id_usuario
AND Usuario.correo='".$correo."'";


$result = pg_query_params($dbconn, $sql, array());
if( pg_num_rows($result) > 0 ) {
    while($row = pg_fetch_assoc($result)) {
        echo '<tr>
            <td>'.$row["id_moneda"].'</td>
            <td>'.$row["sigla"].'</td>
            <td>'.$row["balance"].'</td>
            <td>'.$row["valor"].' USD</td>
            <td>'.$row["balance"]*$row["valor"].' USD</td>
        </tr>';
    }
    pg_close($dbconn);
} else {
    echo "Hubo un error al solicitar los datos";
    pg_close($dbconn);
}
?>